package org.podpage.mobdisguise;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	/**
	 * DON'T SHARE THIS CODE! DON'T SELL THIS CODE! DON'T CHANGE CLASS NAMES!
	 * 
	 * THIS IS CODE FROM PODPAGE (C) 2014 - MIT License
	 * 
	 */

	public static Main plugin;

	public static Main getInstance() {
		return plugin;
	}

	@Override
	public void onLoad() {
		plugin = this;
	}

	@Override
	public void onDisable() {
		PacketManager.unload();
	}

	@Override
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(new PacketManager(), this);
	}
}
