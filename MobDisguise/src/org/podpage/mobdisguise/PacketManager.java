package org.podpage.mobdisguise;

import java.lang.reflect.InvocationTargetException;

import net.minecraft.util.io.netty.channel.Channel;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.podpage.mobdisguise.DisguiseManager.DisguiseType;

public class PacketManager implements Listener {

	public PacketManager() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			addChannelbeforPlayer(p);
		}
	}

	public static void unload() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			removeChannelbeforPlayer(p);
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		addChannelbeforPlayer(e.getPlayer());
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		DisguiseManager.undisguiseandQuit(e.getPlayer());
	}

	@EventHandler
	public void onSneak(PlayerToggleSneakEvent e) {
		if (e.isSneaking()) {
			DisguiseManager.disguise(e.getPlayer(), DisguiseType.COW);
		} else {
			DisguiseManager.undisguise(e.getPlayer());
		}
	}

	public void addChannelbeforPlayer(Player p) {
		try {
			Channel channel = (Channel) Reflection.getChannel(p);
			channel.pipeline().addBefore("packet_handler", "DiguiseManager", new MyChannelDuplexHandler(p));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void removeChannelbeforPlayer(Player p) {
		try {
			Channel channel = (Channel) Reflection.getChannel(p);
			channel.pipeline().remove("DiguiseManager");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void sendPacket(Player p, Object packet) {
		try {
			Object connection = Reflection.getConnection(p);
			Reflection.getMethod(connection.getClass(), "sendPacket").invoke(connection, packet);
		} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	public static int getVersion(Player p) {
		try {
			final Object network = Reflection.getNetworkManager(p);
			final Object channel = Reflection.getField(network.getClass(), "m").get(network);
			final Object version = Reflection.getMethod(network.getClass(), "getVersion", Channel.class).invoke(network, channel);
			return (int) version;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
}
