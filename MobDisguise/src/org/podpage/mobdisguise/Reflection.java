package org.podpage.mobdisguise;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.bukkit.Bukkit;

public class Reflection {

	public static void setValue(Object packet, String s, Object fieldcontent) {
		try {
			Field field = packet.getClass().getDeclaredField(s);
			field.setAccessible(true);
			field.set(packet, fieldcontent);
			field.setAccessible(false);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static Object getValue(Object packet, String s) {
		try {
			Field field = packet.getClass().getDeclaredField(s);
			field.setAccessible(true);
			return field.get(packet);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static Class<?> getMinecraftClass(String name) throws ClassNotFoundException {
		String classname = "net.minecraft.server." + Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + "."
				+ name;
		return Class.forName(classname);
	}

	public static Class<?> getBukkitClass(String name) throws ClassNotFoundException {
		String classname = "org.bukkit.craftbukkit." + Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + "."
				+ name;
		return Class.forName(classname);
	}

	public static String getVersion() {
		String name = Bukkit.getServer().getClass().getPackage().getName();
		String version = name.substring(name.lastIndexOf('.') + 1) + ".";
		return version;
	}

	public static Object getHandle(Object obj) {
		try {
			return getMethod(obj.getClass(), "getHandle", new Class[0]).invoke(obj, new Object[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Object getConnection(Object obj) throws IllegalArgumentException, IllegalAccessException {
		Object handle = getHandle(obj);
		return getField(handle.getClass(), "playerConnection").get(handle);
	}

	public static Object getNetworkManager(Object obj) throws IllegalArgumentException, IllegalAccessException {
		Object connection = getConnection(obj);
		return getField(connection.getClass(), "networkManager").get(connection);
	}

	public static Object getChannel(Object obj) throws IllegalArgumentException, IllegalAccessException {
		Object networkmanager = getNetworkManager(obj);
		return getField(networkmanager.getClass(), "m").get(networkmanager);
	}

	public static Field getField(Class<?> clazz, String name) {
		try {
			Field field = clazz.getDeclaredField(name);
			field.setAccessible(true);
			return field;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Method getMethod(Class<?> clazz, String name, Class<?>... args) {
		for (Method m : clazz.getMethods()) {
			if ((m.getName().equals(name)) && ((args.length == 0) || ClassListEqual(args, m.getParameterTypes()))) {
				m.setAccessible(true);
				return m;
			}
		}
		return null;
	}

	public static boolean ClassListEqual(Class<?>[] l1, Class<?>[] l2) {
		boolean equal = true;
		if (l1.length != l2.length) {
			return false;
		}
		for (int i = 0; i < l1.length; i++) {
			if (l1[i] != l2[i]) {
				equal = false;
				break;
			}
		}
		return equal;
	}
}