package org.podpage.mobdisguise;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class DisguiseManager {

	private static HashMap<String, DisguiseType> disguiselist = new HashMap<>();

	public static void disguise(final Player p, DisguiseType type) {
		undisguise(p);
		disguiselist.put(p.getName(), type);
		for (final Player t : Bukkit.getOnlinePlayers()) {
			if (t != p) {
				t.hidePlayer(p);
				Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {

					@Override
					public void run() {
						t.showPlayer(p);
					}
				}, 5);
			}
		}
	}

	public static void undisguise(final Player p) {
		if (isDisguised(p)) {
			disguiselist.remove(p.getName());
			for (final Player t : Bukkit.getOnlinePlayers()) {
				if (t != p) {
					t.hidePlayer(p);
					Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {

						@Override
						public void run() {
							t.showPlayer(p);
						}
					}, 5);
				}
			}
		}
	}

	public static void undisguiseandQuit(Player p) {
		if (isDisguised(p)) {
			disguiselist.remove(p.getName());
		}
	}

	public static DisguiseType getPlayerDisguiseType(Player p) {
		if (isDisguised(p)) {
			return disguiselist.get(p.getName());
		} else {
			return null;
		}
	}

	public static boolean isDisguised(Player p) {
		return disguiselist.containsKey(p.getName());
	}

	public static DisguiseType getPlayerDisguiseType(String name) {
		if (isDisguised(name)) {
			return disguiselist.get(name);
		} else {
			return null;
		}
	}

	public static boolean isDisguised(String name) {
		return disguiselist.containsKey(name);
	}

	private static Object getDestroyPacket(Player p) {

		try {
			return Reflection.getMinecraftClass("PacketPlayOutEntityDestroy").getConstructor(int[].class).newInstance(new int[] { p.getEntityId() });
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static Object getPlayerPacket(Player p) {

		try {
			return Reflection.getMinecraftClass("PacketPlayOutNamedEntitySpawn").getConstructor(Reflection.getMinecraftClass("EntityHuman"))
					.newInstance(Reflection.getHandle(p));
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public enum DisguiseType {
		CREEPER(50), SKELETON(51), WITHERSKELETON(51), ZOMBIE(54), SMALLZOMBIE(54), SPIDER(52), GIANT(53), SMALLSLIME(55), SLIME(55), BIGSILME(55), GIANTSLIME(
				55), GHAST(56), ANGRYGHAST(56), PIGZOMBIE(57), ENDERMAN(58), CAVESPIDER(59), SILVERFISH(60), BLAZE(61), SMALLMAGMACUBE(62), MAGMACUBE(
				62), BIGMAGMACUBE(62), GIANTMAGMACUBE(62), WITHER(64), BAT(65), WITCH(66), PIG(90), BABYPIG(90), SHEEP(91), BABYSHEEP(91), COW(92), BABYCOW(
				92), CHICKEN(93), BABYCHICKEN(93), SQUID(94), WOLF(95), BABYWOLF(95), MOOSHROOMCOW(96), BABYMOOSHROOMCOW(96), SNOWMAN(97), OCELOT(98), BABYOCELOT(
				98), CAT(98), BABYCAT(98), IRONGOLEM(99), HORSE(100), BABYHORSE(100), DONKEYHORSE(100), BABYDONKEYHORSE(100), MULEHORSE(100), BABYMULEHORSE(
				100), SKELETONHORSE(100), BABYSKELETONHORSE(100), ZOMBIEHORSE(100), BABYZOMBIEHORSE(100), VILLAGER(120), BABYVILLAGER(120), ENDERMITE(
				67, true), GUARDIAN(68, true), ELDERGUARDIAN(68, true), RABBIT(101, true), BABYRABBIT(101, true);

		private byte b;
		private boolean is18;

		DisguiseType(int i) {
			b = (byte) i;
			is18 = false;
		}

		DisguiseType(int i, boolean is18) {
			b = (byte) i;
			this.is18 = is18;
		}

	}

	public static Object getPacket(Player p, Player t, DisguiseType type) {
		Object packet = null;
		try {
			packet = Reflection.getMinecraftClass("PacketPlayOutSpawnEntityLiving").getConstructor().newInstance();

			Reflection.setValue(packet, "a", p.getEntityId());

			if (type.is18) {
				if (PacketManager.getVersion(t) < 47) {
					type = DisguiseType.ZOMBIE;
				}
			}
			Reflection.setValue(packet, "b", type.b);
			Reflection.setValue(packet, "c", floor(p.getLocation().getX() * 32.0D));
			Reflection.setValue(packet, "d", floor(p.getLocation().getY() * 32.0D));
			Reflection.setValue(packet, "e", floor(p.getLocation().getZ() * 32.0D));

			Reflection.setValue(packet, "i", (byte) (int) (p.getLocation().getYaw() * 256.0F / 360.0F));
			Reflection.setValue(packet, "j", (byte) (int) (p.getLocation().getPitch() * 256.0F / 360.0F));

			Reflection.setValue(packet, "k", (byte) 1);

			Object dw = Reflection.getMinecraftClass("DataWatcher").getConstructor(Reflection.getMinecraftClass("Entity"))
					.newInstance(Reflection.getHandle(p));

			Method dwm = Reflection.getMethod(dw.getClass(), "a", int.class, Object.class);

			dwm.invoke(dw, 0, (byte) 0);
			dwm.invoke(dw, 1, (short) 300);
			dwm.invoke(dw, 2, "�7" + p.getName());
			dwm.invoke(dw, 3, (byte) 1);
			dwm.invoke(dw, 6, Float.valueOf(20.0F));
			dwm.invoke(dw, 7, Integer.valueOf(0));
			dwm.invoke(dw, 8, (byte) 0);
			dwm.invoke(dw, 9, (byte) 0);
			dwm.invoke(dw, 10, "�7" + p.getName());
			dwm.invoke(dw, 11, (byte) 1);

			if (type.name().contains("BABY")) {
				if (PacketManager.getVersion(t) >= 47) {
					dwm.invoke(dw, 12, (byte) -1);
				} else {
					if (type.name().contains("HORSE")) {
						dwm.invoke(dw, 12, (int) -20000);
					} else {
						dwm.invoke(dw, 12, (int) -1);
					}
				}
			}
			if (type.name().contains("HORSE")) {
				dwm.invoke(dw, 16, (int) 0);
				dwm.invoke(dw, 20, (int) 0);
				dwm.invoke(dw, 21, p.getName());
				dwm.invoke(dw, 22, (int) 0);
			}
			if (type.equals(DisguiseType.BAT)) {
				dwm.invoke(dw, 16, (byte) 0);
			} else if (type.equals(DisguiseType.ANGRYGHAST)) {
				dwm.invoke(dw, 16, (byte) 1);
			} else if (type.equals(DisguiseType.CREEPER)) {
				dwm.invoke(dw, 16, (byte) -1);
				dwm.invoke(dw, 17, (byte) 0);
				dwm.invoke(dw, 18, (byte) 0);
			} else if (type.equals(DisguiseType.DONKEYHORSE) || type.equals(DisguiseType.BABYDONKEYHORSE)) {
				dwm.invoke(dw, 19, (byte) 1);
			} else if (type.equals(DisguiseType.MULEHORSE) || type.equals(DisguiseType.BABYMULEHORSE)) {
				dwm.invoke(dw, 19, (byte) 2);
			} else if (type.equals(DisguiseType.SKELETONHORSE) || type.equals(DisguiseType.BABYSKELETONHORSE)) {
				dwm.invoke(dw, 19, (byte) 3);
			} else if (type.equals(DisguiseType.ZOMBIEHORSE) || type.equals(DisguiseType.BABYZOMBIEHORSE)) {
				dwm.invoke(dw, 19, (byte) 4);
			} else if (type.equals(DisguiseType.SMALLSLIME) || type.equals(DisguiseType.SMALLMAGMACUBE)) {
				dwm.invoke(dw, 16, (byte) 1);
			} else if (type.equals(DisguiseType.SLIME) || type.equals(DisguiseType.MAGMACUBE)) {
				dwm.invoke(dw, 16, (byte) 2);
			} else if (type.equals(DisguiseType.BIGSILME) || type.equals(DisguiseType.BIGMAGMACUBE)) {
				dwm.invoke(dw, 16, (byte) 4);
			} else if (type.equals(DisguiseType.GIANTSLIME) || type.equals(DisguiseType.GIANTMAGMACUBE)) {
				dwm.invoke(dw, 16, (byte) 10);
			} else if (type.equals(DisguiseType.WOLF) || type.equals(DisguiseType.BABYWOLF)) {
				dwm.invoke(dw, 16, (byte) 0x04);
				dwm.invoke(dw, 18, (float) 20);
				dwm.invoke(dw, 19, (byte) 0);
				dwm.invoke(dw, 20, (byte) 0);
			} else if (type.equals(DisguiseType.CAT) || type.equals(DisguiseType.BABYCAT)) {
				dwm.invoke(dw, 18, (byte) 1);
			} else if (type.equals(DisguiseType.GUARDIAN)) {
				dwm.invoke(dw, 16, (int) 0);
				dwm.invoke(dw, 17, (int) 0);
			} else if (type.equals(DisguiseType.ELDERGUARDIAN)) {
				dwm.invoke(dw, 16, (int) 4);
				dwm.invoke(dw, 17, (int) 0);
			} else if (type.equals(DisguiseType.WITHERSKELETON)) {
				dwm.invoke(dw, 13, (byte) 1);
			} else if (type.equals(DisguiseType.SMALLZOMBIE)) {
				dwm.invoke(dw, 16, (byte) 1);
			}

			Reflection.setValue(packet, "l", dw);

			double d0 = 3.9D;
			double d1 = p.getVelocity().getX();
			double d2 = p.getVelocity().getY();
			double d3 = p.getVelocity().getZ();

			if (d1 < -d0) {
				d1 = -d0;
			}

			if (d2 < -d0) {
				d2 = -d0;
			}

			if (d3 < -d0) {
				d3 = -d0;
			}

			if (d1 > d0) {
				d1 = d0;
			}

			if (d2 > d0) {
				d2 = d0;
			}

			if (d3 > d0) {
				d3 = d0;
			}

			Reflection.setValue(packet, "f", (int) (d1 * 8000.0D));
			Reflection.setValue(packet, "g", (int) (d2 * 8000.0D));
			Reflection.setValue(packet, "h", (int) (d3 * 8000.0D));
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return packet;
	}

	private static int floor(double paramDouble) {
		int i = (int) paramDouble;
		return ((paramDouble < i) ? i - 1 : i);
	}
}
