package org.podpage.mobdisguise;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.podpage.mobdisguise.DisguiseManager.DisguiseType;

import net.minecraft.util.com.mojang.authlib.GameProfile;
import net.minecraft.util.io.netty.channel.ChannelDuplexHandler;
import net.minecraft.util.io.netty.channel.ChannelHandlerContext;
import net.minecraft.util.io.netty.channel.ChannelPromise;

public class MyChannelDuplexHandler extends ChannelDuplexHandler {

	private Player p;

	public MyChannelDuplexHandler(Player p) {
		this.p = p;
	}

	@Override
	public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
		if (msg.getClass().getSimpleName().equals("PacketPlayOutNamedEntitySpawn")) {
			GameProfile gp = (GameProfile) Reflection.getValue(msg, "b");
			String nick = gp.getName();
			DisguiseType dt = DisguiseManager.getPlayerDisguiseType(nick);
			if (dt != null) {
				Player t = Bukkit.getPlayer(nick);
				msg = DisguiseManager.getPacket(t, p, dt);
			}
		}
		super.write(ctx, msg, promise);
	}
}